﻿namespace Interface
{
    public interface ICurrencyAmount
    {
        string CurrencyCode { get; }
        string TypeCode { get; }
        decimal Amount { get; }
    }
}
