﻿using System;
using System.Linq;

namespace Interface
{
    public class BudgetApplication : IBudgetApplication
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionParser _transactionParser;
        private readonly ICurrencyConverter _currencyConverter;

        public BudgetApplication(ITransactionRepository transactionRepository,
            ITransactionParser transactionParser,
            ICurrencyConverter currencyConverter)
        {
            _transactionRepository = transactionRepository;
            _transactionParser = transactionParser;
            _currencyConverter = currencyConverter;
        }

        public void AddTransaction(string input)
        {
            var transaction = _transactionParser.Parse(input);
            _transactionRepository.AddTransaction(transaction);
        }

        public void OutputBalanceInCurrency(string currencyCode)
        {
            var transactions = _transactionRepository.GetTransactions();

            var balance = transactions
               .Select(x => x.Amount)
               .Select(x => x.CurrencyCode == currencyCode ? x
                : _currencyConverter.ConvertCurrency(x, currencyCode))
               .Sum(x => x.Amount);

            Console.WriteLine($"Balance: {balance:0.00} {currencyCode}");
        }

        public void OutputTransactions()
        {
            var transactions = _transactionRepository.GetTransactions();

            foreach (var tr in transactions)
            {
                Console.WriteLine(tr.ToString());
            }
        }
    }
}
