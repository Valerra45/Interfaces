﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Interface
{
    public class FileTransactionRepository : ITransactionRepository
    {
        private const string filePath = @"transactions.txt";
        private readonly List<ITransaction> _transactions = new List<ITransaction>();

        public void AddTransaction(ITransaction transaction)
        {
            using (StreamWriter sw = new StreamWriter(filePath, true, Encoding.Default))
            {
                sw.WriteLine(JsonConvert.SerializeObject(transaction));
            }
        }

        public ITransaction[] GetTransactions()
        {
            if (File.Exists(filePath))
            {
                using (StreamReader sr = new StreamReader(filePath, Encoding.Default))
                {
                    string json;
                    while ((json = sr.ReadLine()) != null)
                    {
                        _transactions.Add(JsonConvert.DeserializeObject<Expense>(json));
                    }
                }
            }

            return _transactions.ToArray();
        }
    }
}
