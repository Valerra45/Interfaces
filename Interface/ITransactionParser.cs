﻿namespace Interface
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}
