﻿using System;
using System.Collections.Generic;

namespace Interface
{
    public class CurrencyAmount : ICurrencyAmount, IEquatable<CurrencyAmount>
    {
        public CurrencyAmount(string currencyCode, decimal amount, string typeCode)
        {
            CurrencyCode = currencyCode;
            Amount = amount;
            TypeCode = typeCode;
        }

        public string CurrencyCode { get; }

        public string TypeCode { get; }

        public decimal Amount { get; private set; }

        public static CurrencyAmount operator +(CurrencyAmount x, ICurrencyAmount y)
        {
            if (x.CurrencyCode != y.CurrencyCode)
            {
                throw new InvalidOperationException("Currencies should be equal");
            }
            return new CurrencyAmount(x.CurrencyCode, x.Amount + y.Amount, x.TypeCode);
        }

        public static CurrencyAmount operator -(CurrencyAmount x, ICurrencyAmount y)
        {
            if (x.CurrencyCode != y.CurrencyCode)
            {
                throw new InvalidOperationException("Currencies should be equal");
            }
            return new CurrencyAmount(x.CurrencyCode, x.Amount - y.Amount, x.TypeCode);
        }

        public static bool operator ==(CurrencyAmount left, CurrencyAmount right)
        {
            return EqualityComparer<CurrencyAmount>.Default.Equals(left, right);
        }

        public static bool operator !=(CurrencyAmount left, CurrencyAmount right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"{TypeCode} {Math.Abs(Amount):0.00} {CurrencyCode}";
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CurrencyAmount);
        }

        public bool Equals(CurrencyAmount other)
        {
            return other != null &&
                   CurrencyCode == other.CurrencyCode &&
                   Amount == other.Amount &&
                   TypeCode == other.TypeCode;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CurrencyCode, Amount);
        }
    }
}
