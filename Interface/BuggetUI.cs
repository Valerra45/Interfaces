﻿using System;
using System.Threading;

namespace Interface
{
    public class BuggetUI : IBudgetUI
    {
        private readonly BudgetApplication _budgetApplication;
        private string[] _menu;
        private int _arrow;

        public BuggetUI(BudgetApplication budgetApplication)
        {
            _budgetApplication = budgetApplication;
            _arrow = 0;
            _menu = new string[]
            {
                "Add transaction",
                "Transactions list",
                "Balance",
                "Exit"
            };
        }

        private void Add()
        {
            Console.Clear();
            Console.WriteLine("Transaction type: Rate, Income, Transfer.");
            Console.WriteLine("Enter transaction (transaction type/amount/currency/assignment/name):");

            var transaction = Console.ReadLine();

            _budgetApplication.AddTransaction(transaction);

            Console.WriteLine();
            Console.WriteLine("Transaction added.");
            Thread.Sleep(1000);
        }

        private void Balance()
        {
            Console.Clear();
            Console.WriteLine("Enter currency code:");
            var currencyCode = Console.ReadLine();

            Console.Clear();
            Console.WriteLine("Balance:");
            Console.WriteLine();

            _budgetApplication.OutputBalanceInCurrency(currencyCode);

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private void List()
        {
            Console.Clear();
            Console.WriteLine("Transactions list:");
            Console.WriteLine();

            _budgetApplication.OutputTransactions();

            Console.WriteLine();
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        public void Run()
        {
            while (true)
            {
                Console.Clear();

                if (_arrow < (int)Menu.Add)
                {
                    _arrow = (int)Menu.Exit;
                }
                else if (_arrow > (int)Menu.Exit)
                {
                    _arrow = (int)Menu.Add;
                }

                for (int i = 0; i < _menu.Length; i++)
                {
                    Console.WriteLine($"{(_arrow == i ? ">>" : "  ")} {_menu[i]}");
                }

                ConsoleKey key = Console.ReadKey().Key;

                if (key == ConsoleKey.UpArrow)
                {
                    _arrow--;
                }
                else if (key == ConsoleKey.DownArrow)
                {
                    _arrow++;
                }
                else if (key == ConsoleKey.Enter)
                {
                    switch ((Menu)_arrow)
                    {
                        case Menu.Add:
                            Add();
                            break;
                        case Menu.List:
                            List();
                            break;
                        case Menu.Balanse:
                            Balance();
                            break;
                        case Menu.Exit:
                            Console.Clear();
                            return;
                    }
                }

            }
        }

        private enum Menu
        {
            Add,
            List,
            Balanse,
            Exit
        }
    }
}
