﻿using System;

namespace Interface
{
    public class Expense : ITransaction
    {
        public Expense(CurrencyAmount amount,
            DateTimeOffset date,
            string category,
            string destination)
        {
            Amount = amount;
            Date = date;
            Category = category;
            Destination = destination;
        }

        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }
        public string Category { get; }
        public string Destination { get; }

        public override string ToString()
            => $"{Amount} in {Destination} category {Category}";
    }
}
