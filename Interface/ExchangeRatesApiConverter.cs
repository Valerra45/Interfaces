﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Interface
{
    public class ExchangeRatesApiConverter : ICurrencyConverter
    {
        private readonly HttpClient _httpClient;
        private readonly IMemoryCache _memoryCache;
        private readonly string _apiKey;

        public ExchangeRatesApiConverter(HttpClient httpClient,
            IMemoryCache memoryCache,
            string apiKey)
        {
            _httpClient = httpClient;
            _memoryCache = memoryCache;
            _apiKey = apiKey;
        }

        public async Task<ICurrencyAmount> ConvertCurrencyAsync(ICurrencyAmount amount, string currencyCode)
        {
            var cachedResponse = await _memoryCache.GetOrCreateAsync("response", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1);
                return GetExchangeRatesAsync();
            });

            var amountBase = amount.Amount / (decimal)cachedResponse.Rates[amount.CurrencyCode];
            var targetAmount = amountBase * (decimal)cachedResponse.Rates[currencyCode];

            return new CurrencyAmount(currencyCode, targetAmount, amount.TypeCode);
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            return Task.Run(async () =>
                await ConvertCurrencyAsync(amount, currencyCode))
                .Result;
        }

        private async Task<ExchangeRatesApiResponse> GetExchangeRatesAsync()
        {
            var response = await _httpClient.GetAsync($"http://api.exchangeratesapi.io/v1/latest?access_key={_apiKey}");
            response = response.EnsureSuccessStatusCode();
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ExchangeRatesApiResponse>(json);
        }

        public class ExchangeRatesApiResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("timestamp")]
            public long Timestamp { get; set; }

            [JsonProperty("base")]
            public string Base { get; set; }

            [JsonProperty("date")]
            public DateTimeOffset Date { get; set; }

            [JsonProperty("rates")]
            public Dictionary<string, double> Rates { get; set; }
        }
    }
}
