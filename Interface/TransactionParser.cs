﻿using System;

namespace Interface
{
    public class TransactionParser : ITransactionParser
    {
        public ITransaction Parse(string input)
        {
            var date = DateTimeOffset.Now;
            var splits = input.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            var typeCode = splits[0];
            decimal amount;

            if (typeCode == "Rate" || typeCode == "Transfer")
            {
                amount = -decimal.Parse(splits[1]);
            }
            else if (typeCode == "Income")
            {
                amount = decimal.Parse(splits[1]);
            }
            else
            {
                throw new NotImplementedException();
            }

            var currencyAmount = new CurrencyAmount(splits[2], amount, typeCode);

            return new Expense(currencyAmount, date, splits[3], splits[4]);
        }
    }
}
